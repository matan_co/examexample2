<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
		
		// add the rule
		$rule = new \app\rbac\UpdateOwnUserRule;
		$auth->add($rule);
		
		$rule_manager = new \app\rbac\ManagerCanViewOnlyRule;
		$auth->add($rule_manager);
		
        // add "create Breakdown" permission
        $createBreakdown = $auth->createPermission('createBreakdown');
        $auth->add($createBreakdown);
		
		// add "updateBreakdown" permission
        $updateBreakdown = $auth->createPermission('updateBreakdown');
        $auth->add($updateBreakdown);
		
		// add "deleteBreakdown" permission
        $deleteBreakdown = $auth->createPermission('deleteBreakdown');
        $auth->add($deleteBreakdown);
		
		// add "viewBreakdown" permission
        $viewBreakdown = $auth->createPermission('viewBreakdown');
        $auth->add($viewBreakdown);
		
		// add "createUser" permission
        $createUser = $auth->createPermission('createUser');
        $auth->add($createUser);
		
		// add "updateUser" permission
        $updateUser = $auth->createPermission('updateUser');
        $auth->add($updateUser);
		
		// add "viewUser" permission
        $viewUser = $auth->createPermission('viewUser');
        $auth->add($viewUser);
		
		// add "deleteUser" permission
        $deleteUser = $auth->createPermission('deleteUser');
        $auth->add($deleteUser);
		
		// add the "updateOwnUser" permission and associate the rule with it.
		$updateOwnUser = $auth->createPermission('updateOwnUser');
		$updateOwnUser->ruleName = $rule->name;
		$auth->add($updateOwnUser);
		
		// add the "viewOnlyUrgent" permission and associate the rule with it.
		$viewOnlyUrgent = $auth->createPermission('viewOnlyUrgent');
		$viewOnlyUrgent->ruleName = $rule_manager->name;
		$auth->add($viewOnlyUrgent);
		
		// roles
		$admin = $auth->createRole('admin');
		$auth->add($admin);
		$manager = $auth->createRole('manager');
		$auth->add($manager);
		$teamleader = $auth->createRole('teamleader');
		$auth->add($teamleader);
		$member = $auth->createRole('member');
		$auth->add($member);
		$not_authorized = $auth->createRole('not_authorized');
		$auth->add($not_authorized);
		
		// assign roles
		$auth->addChild($member, $createBreakdown);
		$auth->addChild($member, $viewBreakdown);
		$auth->addChild($updateOwnUser, $updateUser);
		$auth->addChild($member, $updateOwnUser);
		$auth->addChild($teamleader, $updateBreakdown);
		$auth->addChild($viewOnlyUrgent, $updateBreakdown);
		$auth->addChild($manager, $viewOnlyUrgent);
		$auth->addChild($admin, $deleteBreakdown);
		$auth->addChild($admin, $updateUser);
		$auth->addChild($admin, $viewUser);
		$auth->addChild($admin, $deleteUser);
		
		// Hirarchy
		$auth->addChild($admin, $manager);
		$auth->addChild($manager, $teamleader);
		$auth->addChild($teamleader, $member);
		$auth->addChild($member, $not_authorized);
		

    }
}